const INPUT_GPX = "eppchez.gpx";

class GPSKalmanFilter {
  // https://stackoverflow.com/questions/1134579/smooth-gps-data
  // HDOP/VDOP: https://en.wikipedia.org/wiki/Dilution_of_precision_(navigation)
  constructor(decay = 3) {
    this.decay = decay;
    this.variance = -1;
    this.minAccuracy = 1;
  }

  process(lat, lng, accuracy, timestampInMs) {
    if (accuracy < this.minAccuracy) accuracy = this.minAccuracy;

    if (this.variance < 0) {
      this.timestampInMs = timestampInMs;
      this.lat = lat;
      this.lng = lng;
      this.variance = accuracy * accuracy;
    } else {
      const timeIncMs = timestampInMs - this.timestampInMs;

      if (timeIncMs > 0) {
        this.variance += (timeIncMs * this.decay * this.decay) / 1000;
        this.timestampInMs = timestampInMs;
      }

      const _k = this.variance / (this.variance + accuracy * accuracy);
      this.lat += _k * (lat - this.lat);
      this.lng += _k * (lng - this.lng);

      this.variance = (1 - _k) * this.variance;
    }

    return [this.lng, this.lat];
  }
}

const parseGPX = (xmlDoc) => {
  let coordinates = [];
  for (let idx = 0; idx < xmlDoc.getElementsByTagName("wpt").length; idx++) {
    const el = xmlDoc.getElementsByTagName("wpt")[idx];
    coordinates.push({
      lng: parseFloat(el.getAttribute("lon")),
      lat: parseFloat(el.getAttribute("lat")),
      vdop: parseFloat(el.getElementsByTagName("vdop")[0].textContent), //VDOP: vertical dilution of precision
      hdop: parseFloat(el.getElementsByTagName("hdop")[0].textContent), //HDOP: horizontal dilution of precision
      ele: parseFloat(el.getElementsByTagName("ele")[0].textContent),
      magvar: parseFloat(el.getElementsByTagName("ele")[0].textContent),
      speed: parseFloat(el.getElementsByTagName("speed")[0].textContent),
      timestampInMs: idx * 1000,
    });
  }
  return coordinates;
};

fetch(INPUT_GPX)
  .then((response) => response.text())
  .then((str) => new window.DOMParser().parseFromString(str, "text/xml"))
  .then((data) => {
    const coords = parseGPX(data);
    const kalmanFilter = new GPSKalmanFilter();
    const filteredCoords = [];
    let newGpx = "<gpx>\n";
    coords.forEach((coord, idx) => {
      const { lat, lng, hdop, timestampInMs, ele, magvar, speed } = coord;
      filteredCoords.push(kalmanFilter.process(lat, lng, hdop, timestampInMs));

      let row = document.createElement("tr");
      const col1 = document.createElement("td");
      const col2 = document.createElement("td");
      col1.innerHTML = `${coord.lat}, ${coord.lng}`;
      col2.innerHTML = `${filteredCoords[idx][1]}, ${filteredCoords[idx][0]}`;
      row.appendChild(col1);
      row.appendChild(col2);
      document.getElementById("coordinateTable").appendChild(row);

      newGpx += `<wpt lon="${filteredCoords[idx][0]}" lat="${filteredCoords[idx][1]}"><ele>${ele}</ele><magvar>${magvar}</magvar><speed>${speed}</speed></wpt>\n`;
    });
    newGpx += "</gpx>";

    document.getElementById(
      "gpxContent"
    ).value = new XMLSerializer().serializeToString(data);

    document.getElementById("gpxContentFiltered").value = newGpx;
  });
